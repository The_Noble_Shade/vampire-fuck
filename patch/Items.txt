> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
ポーション
> CONTEXT: Items/1/name/ < UNTRANSLATED
> CONTEXT: Items/80/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ＨＰを５００ポイント回復する。
> CONTEXT: Items/1/description/ < UNTRANSLATED
> CONTEXT: Items/80/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ハイポーション
> CONTEXT: Items/2/name/ < UNTRANSLATED
> CONTEXT: Items/81/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ＨＰを２５００ポイント回復する。
> CONTEXT: Items/2/description/ < UNTRANSLATED
> CONTEXT: Items/81/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
フルポーション
> CONTEXT: Items/3/name/ < UNTRANSLATED
> CONTEXT: Items/82/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ＨＰを完全に回復する。
> CONTEXT: Items/3/description/ < UNTRANSLATED
> CONTEXT: Items/82/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
マジックウォーター
> CONTEXT: Items/4/name/ < UNTRANSLATED
> CONTEXT: Items/83/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ＭＰを２００ポイント回復する。
> CONTEXT: Items/4/description/ < UNTRANSLATED
> CONTEXT: Items/83/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
マジカルウォーターⅡ
> CONTEXT: Items/5/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ＭＰを５００ポイント回復する。
> CONTEXT: Items/5/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ミラクルウォーターⅢ
> CONTEXT: Items/6/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ＭＰを完全に回復する。
> CONTEXT: Items/6/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
エリクサー
> CONTEXT: Items/7/name/ < UNTRANSLATED
> CONTEXT: Items/87/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ＨＰとＭＰを完全に回復する。
> CONTEXT: Items/7/description/ < UNTRANSLATED
> CONTEXT: Items/87/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
苦い薬
> CONTEXT: Items/8/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ムラムラや発情状態を解除する苦～い薬。
> CONTEXT: Items/8/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ミンミン打破
> CONTEXT: Items/10/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
眠っている者を叩き起こすドリンク。
> CONTEXT: Items/10/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
睡眠薬
> CONTEXT: Items/11/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
戦闘中に眠る事ができるアイテム。
> CONTEXT: Items/11/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
スルスルローション
> CONTEXT: Items/15/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ダンジョンから脱出できるアイテム。
> CONTEXT: Items/15/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ビンビンスプレー
> CONTEXT: Items/16/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
付近にいる敵を呼び出す香水。
> CONTEXT: Items/16/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
淫乱ジュース
> CONTEXT: Items/17/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
戦闘中使用すると５ターンの間攻撃力が上がる。
> CONTEXT: Items/17/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
エンゲージリング♂
> CONTEXT: Items/19/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
フェリシアさんから渡された婚約指輪。
> CONTEXT: Items/19/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
▼キーアイテム▼
> CONTEXT: Items/20/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
物置の合鍵
> CONTEXT: Items/21/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
トランシルヴァーギナ城１階、物置の合鍵。
> CONTEXT: Items/21/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
秘蔵の写真
> CONTEXT: Items/22/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
\\N[1]の写真。モリィのコレクションの１枚。
> CONTEXT: Items/22/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
売り物
> CONTEXT: Items/23/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
旅の吸血鬼から盗まれた、お店の商品。
> CONTEXT: Items/23/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ゾンビヘッド
> CONTEXT: Items/24/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
うっかり屋のゾンビが落とした頭。
> CONTEXT: Items/24/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ヴァーギナ地下道の鍵
> CONTEXT: Items/25/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
町へ通じるヴァーギナ地下道へ入るための鍵。
> CONTEXT: Items/25/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
盗まれたお供え物
> CONTEXT: Items/26/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
グルメ家の幽霊の墓に供えてあった食べ物。好物らしい……
> CONTEXT: Items/26/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
想い出の花
> CONTEXT: Items/27/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
おじいさんに頼まれたお供えの花。
> CONTEXT: Items/27/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ビンビンハーブ
> CONTEXT: Items/28/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
おばぁさんが探しているハーブ。
> CONTEXT: Items/28/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
薬草
> CONTEXT: Items/29/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
アビーから依頼された薬草。
> CONTEXT: Items/29/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
重要な書類
> CONTEXT: Items/30/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
置き忘れられた大切な書類。
> CONTEXT: Items/30/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
営業許可証（コピー）
> CONTEXT: Items/31/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
アダルトショップの営業許可証…のコピー。
> CONTEXT: Items/31/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
空瓶
> CONTEXT: Items/32/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
空っぽの瓶。
> CONTEXT: Items/32/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
乙女草のしずく
> CONTEXT: Items/33/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
泉のほとりで採取したしずく。
> CONTEXT: Items/33/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
アメジスト
> CONTEXT: Items/34/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
採取を依頼されたキレイな石。
> CONTEXT: Items/34/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
おばぁちゃんへの手紙
> CONTEXT: Items/35/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
女の子から預かったお手紙。
> CONTEXT: Items/35/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
裏地下道の鍵
> CONTEXT: Items/36/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
城へ通じるもう１つのルートの鍵。
> CONTEXT: Items/36/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
荷物
> CONTEXT: Items/37/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ペリカンさんが落とした荷物。
> CONTEXT: Items/37/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
砂金
> CONTEXT: Items/38/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ハゼの崖で採掘される砂金。
> CONTEXT: Items/38/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
城の鍵
> CONTEXT: Items/39/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
城の正面玄関の鍵。
> CONTEXT: Items/39/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
魔女の秘薬
> CONTEXT: Items/41/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
トランシルヴァーギナ城内へワープできる使用期間限定アイテム。
> CONTEXT: Items/41/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
★ピンクキャンドル
> CONTEXT: Items/44/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
スキルの封印を解除する。
> CONTEXT: Items/44/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
▼宝箱特典▼
> CONTEXT: Items/48/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ジューシィカード☆N
> CONTEXT: Items/49/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
魔法少女☆ジューシィズのノーマルカード。沢山集めると…
> CONTEXT: Items/49/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ジューシィカード☆R
> CONTEXT: Items/50/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
魔法少女☆ジューシィズのレアカード。沢山集めると…
> CONTEXT: Items/50/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ジューシィカード☆SR
> CONTEXT: Items/51/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
魔法少女☆ジューシィズのＳレアカード。沢山集めると…
> CONTEXT: Items/51/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ジューシィカード☆LG
> CONTEXT: Items/52/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
魔法少女☆ジューシィズのレジェンドカード。沢山集めると…
> CONTEXT: Items/52/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
睡眠薬★
> CONTEXT: Items/77/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
-100
> CONTEXT: Items/78/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
★架空アイテム
> CONTEXT: Items/78/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
スティミュラント
> CONTEXT: Items/84/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
戦闘不能から復活させる。
> CONTEXT: Items/84/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
アンチドーテ
> CONTEXT: Items/85/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
毒状態を治療する。
> CONTEXT: Items/85/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ディスペルハーブ
> CONTEXT: Items/86/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
戦闘不能以外の状態異常を治療する。
> CONTEXT: Items/86/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ライフアップ
> CONTEXT: Items/88/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
最大ＨＰを５０ポイント上げる。
> CONTEXT: Items/88/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
マナアップ
> CONTEXT: Items/89/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
最大ＭＰを１０ポイント上げる。
> CONTEXT: Items/89/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
パワーアップ
> CONTEXT: Items/90/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
攻撃力を３ポイント上げる。
> CONTEXT: Items/90/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ガードアップ
> CONTEXT: Items/91/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
防御力を３ポイント上げる。
> CONTEXT: Items/91/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
マジックアップ
> CONTEXT: Items/92/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
魔法力を３ポイント上げる。
> CONTEXT: Items/92/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
レジストアップ
> CONTEXT: Items/93/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
魔法防御を３ポイント上げる。
> CONTEXT: Items/93/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
スピードアップ
> CONTEXT: Items/94/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
敏捷性を３ポイント上げる。
> CONTEXT: Items/94/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
ラックアップ
> CONTEXT: Items/95/name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
運を３ポイント上げる。
> CONTEXT: Items/95/description/ < UNTRANSLATED

> END STRING
