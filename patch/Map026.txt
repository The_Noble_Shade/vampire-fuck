> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
クルーセ墓地管理室
> CONTEXT: Map026/display_name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あったわ、これね、重要な書類って」
> CONTEXT: Map026/events/6/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「フェリシアさん！ どうしてここに……？ 」
> CONTEXT: Map026/events/6/pages/0/17/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「あなたが通過した後、地下道への入り口が閉鎖されていたけど
　どういう事かしら？ 」
> CONTEXT: Map026/events/6/pages/0/20/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そういえば……なぜか塞がっちゃたのよね。
　お兄ちゃんかしら？ 」
> CONTEXT: Map026/events/6/pages/0/25/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「そんなはずはないわ。お兄様はあなたに戻ってきて欲しいと
　思っているのに、帰り道を閉ざしたりするわけないでしょう？ 」
> CONTEXT: Map026/events/6/pages/0/29/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃん……帰ってきて欲しい……の？ 」
> CONTEXT: Map026/events/6/pages/0/35/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「当たり前でしょ。大事な妹が手元を離れて
　とても心配しているわ」
> CONTEXT: Map026/events/6/pages/0/38/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「でもファルサーが…… 」
> CONTEXT: Map026/events/6/pages/0/44/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「彼はあまり信用できないわね……私の個人的な意見だけど。
　私とお兄様の婚約にすごく賛成してるしね」
> CONTEXT: Map026/events/6/pages/0/47/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「賛成されちゃ嫌なの？ 」
> CONTEXT: Map026/events/6/pages/0/52/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「ふふ……あなたはまだ子供ね。とにかく、お兄様は
　あなたの帰りを待ってるわ。もう少し、気持ちが落ち着いたら
　考えてみてね。それじゃ」
> CONTEXT: Map026/events/6/pages/0/55/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あっ……待って……！ 」
> CONTEXT: Map026/events/6/pages/0/61/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……地下道の出口は封鎖されてるはずだけど、
　フェリシアさんは どうやってここへ来たのかしら……？ 」
> CONTEXT: Map026/events/6/pages/0/69/Dialogue < UNTRANSLATED

> END STRING
