> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
ヴィクターの部屋
> CONTEXT: Map010/display_name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「食事にするか？ 」
> CONTEXT: Map010/events/2/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
する
> CONTEXT: Map010/events/2/pages/0/6/Choice/0 < UNTRANSLATED

> END STRING

> BEGIN STRING
まだいらない
> CONTEXT: Map010/events/2/pages/0/6/Choice/1 < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「ならば、抱いて下さいお願いしますと頼むのだな」
> CONTEXT: Map010/events/2/pages/0/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「な……っ、なんでよっ！？ 今まで普通に
　……してくれてたじゃない！ 」
> CONTEXT: Map010/events/2/pages/0/14/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「最近生意気だからな、しっかり教育し直してやろう」
> CONTEXT: Map010/events/2/pages/0/18/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「余計なお世話よっ！ ふーんだいいもん！
　食事なんかしなくたって平気だもん！ 」
> CONTEXT: Map010/events/2/pages/0/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「やせ我慢もいつまで続くかな？ 用がないなら自室に戻りなさい」
> CONTEXT: Map010/events/2/pages/0/26/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「そうか。空腹に見えるが、用がないなら自室に戻りなさい」
> CONTEXT: Map010/events/2/pages/0/32/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「話って…………な、なぁにお兄ちゃん？ 」
> CONTEXT: Map010/events/2/pages/1/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「お前もそろそろ落ち着いて将来を考える時期だな」
> CONTEXT: Map010/events/2/pages/1/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「え？ じゃあ……外に行ってもいいの？ 」
> CONTEXT: Map010/events/2/pages/1/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「ロイドとお前を婚約させる事にした。
　これからは少しは退屈しないだろう」
> CONTEXT: Map010/events/2/pages/1/14/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「一族の繁栄も考えなくては ならんしな」
> CONTEXT: Map010/events/2/pages/1/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「え……そんなっ、勝手に決めないでよ！ 私は…… 」
> CONTEXT: Map010/events/2/pages/1/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「僕じゃ頼りないかな……？ もし今はそうでも、これから
　もっと努力するよ！ 君のために！！ 」
> CONTEXT: Map010/events/2/pages/1/30/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そういう意味じゃなくて…… 」
> CONTEXT: Map010/events/2/pages/1/36/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「なにが不満だ？ 」
> CONTEXT: Map010/events/2/pages/1/39/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……なっ、なんで私だけ将来とか、婚約とかっ！
　結婚なんか……お兄ちゃんがすればいいでしょ！！ 」
> CONTEXT: Map010/events/2/pages/1/43/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「私はすでに婚約済みだ」
> CONTEXT: Map010/events/2/pages/1/47/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「へっ……！？ 」
> CONTEXT: Map010/events/2/pages/1/51/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……そんなの……聞いてない…… 」
> CONTEXT: Map010/events/2/pages/1/60/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「今初めて言ったからな」
> CONTEXT: Map010/events/2/pages/1/63/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……相手は…………誰……？ 」
> CONTEXT: Map010/events/2/pages/1/66/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「……フェリシアだ」
> CONTEXT: Map010/events/2/pages/1/69/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そう……そうよね、フェリシアさん……大人っぽくて優しくて
　素敵だし……彼女なら…… 」
> CONTEXT: Map010/events/2/pages/1/72/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「\\N[1]ちゃん…… 」
> CONTEXT: Map010/events/2/pages/1/77/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/125/337/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「納得したか？ 一族の血を絶やさないのは掟だからな」
> CONTEXT: Map010/events/2/pages/1/80/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お……お兄ちゃんなんか……っ、もう知らない！ 」
> CONTEXT: Map010/events/2/pages/1/93/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「本当にこれで良かったのですか？ 」
> CONTEXT: Map010/events/2/pages/1/116/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「\\N[1]のためだ、仕方がなかろう…… 」
> CONTEXT: Map010/events/2/pages/1/119/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「そう不貞腐れるな。日が経てばお前も
 受け入れられるようになるだろう」
> CONTEXT: Map010/events/2/pages/2/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ふん…… 」
> CONTEXT: Map010/events/2/pages/2/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「なんだ、私に何か用か？
　腹が減ったなら、相手はしてやっても構わないぞ」
> CONTEXT: Map010/events/2/pages/3/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「よ、余計なお世話よ！ 」
> CONTEXT: Map010/events/2/pages/3/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「服を着ろ、服を！ そんな誘い方で乗ると思うのか！？ 」
> CONTEXT: Map010/events/2/pages/5/5/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「なんだその はしたない格好は！ プライドはないのか！？ 」
> CONTEXT: Map010/events/2/pages/5/16/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「フェリシアがドレスの用意をしてるぞ。
　お前は何でも似合うから、楽しみだな」
> CONTEXT: Map010/events/2/pages/5/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「えへへ、そうかな？ 」
> CONTEXT: Map010/events/2/pages/5/32/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「収まるべき所に収まって私も安心だ。これからも兄として
　見守っていくからな」
> CONTEXT: Map010/events/2/pages/5/40/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「うん……ありがとう、お兄ちゃん」
> CONTEXT: Map010/events/2/pages/5/45/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「旅を通してモリィとの絆が深まったのだな。モリィを傍に
　置いて良かった……おかげで無事に城に戻ったしな」
> CONTEXT: Map010/events/2/pages/5/53/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「しかしこれからは「家来」ではないな……少し複雑だ」
> CONTEXT: Map010/events/2/pages/5/57/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あら\\I[122] モリィはいつまでも私の家来よ\\I[122] 」
> CONTEXT: Map010/events/2/pages/5/61/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「ふっ、そうだな」
> CONTEXT: Map010/events/2/pages/5/64/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「そろそろ城に戻ってこないか？ 」
> CONTEXT: Map010/events/2/pages/5/72/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「いやよ！ 私は自由に暮らしたいの！ 」
> CONTEXT: Map010/events/2/pages/5/76/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「……お前の肉体にとってはちょうどいいかもしれないな。
　食事に不自由する事はなかろう」
> CONTEXT: Map010/events/2/pages/5/79/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「これね…… 」
> CONTEXT: Map010/events/4/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あら、これ…… 」
> CONTEXT: Map010/events/4/pages/0/12/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「私の写真……だけど、フェリシアさんも一緒ね…… 」
> CONTEXT: Map010/events/4/pages/0/17/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「難しそうな本ばっかり…… 」
> CONTEXT: Map010/events/6/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「どうかしたか？ 」
> CONTEXT: Map010/events/6/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「う、ううん！ 何でもないっ！ 」
> CONTEXT: Map010/events/6/pages/0/13/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「これって……何に使うのかしら？ ちょっと悪趣味よね」
> CONTEXT: Map010/events/7/pages/0/4/Dialogue < UNTRANSLATED
> CONTEXT: Map010/events/8/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「これはこれは、お嬢様。お帰りになられるとは…… 」
> CONTEXT: Map010/events/11/pages/0/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ちょっと！ なんでお兄ちゃんを閉じ込めてるの！？
　いったいどういうつもり！？ 」
> CONTEXT: Map010/events/11/pages/0/12/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「ふふふ……兄の愛は偉大ですなぁ。いつも狙っていたのに
　なかなか隙がない」
> CONTEXT: Map010/events/11/pages/0/16/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「何の話……？ 」
> CONTEXT: Map010/events/11/pages/0/21/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「お前の事さ、\\N[1]！ 」
> CONTEXT: Map010/events/11/pages/0/24/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「はっ……！？ 何言って…… 」
> CONTEXT: Map010/events/11/pages/0/28/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「特殊な体質の淫乱な肉体、美しい容姿、中身は
　馬鹿な小娘に過ぎんが、まぁ問題ではない……ゆっくり
　躾ければいいだけの話」
> CONTEXT: Map010/events/11/pages/0/32/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ちょ、ちょっと失礼じゃない！ それよりお兄ちゃんに
　歯向かうなんて、どういうつもりなのよ！？ 」
> CONTEXT: Map010/events/11/pages/0/38/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「兄上様……この城の主……たかが吸血鬼……。
　この私が歯向かうなどという言葉は不適切ですなぁ、お嬢様」
> CONTEXT: Map010/events/11/pages/0/42/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「お嬢様と呼ぶのもそろそろやめにしよう。貴様は
　これから、このファルサー様の可愛い肉奴隷になるのだから」
> CONTEXT: Map010/events/11/pages/0/47/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「なにを勝手な……！ 」
> CONTEXT: Map010/events/11/pages/0/52/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「フン。元々私は吸血鬼一族など目ではないのさ。
　気まぐれで部下として潜り込み、お前たち兄妹をかき乱すのは
　楽しかったが…… 」
> CONTEXT: Map010/events/11/pages/0/55/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「一途であった娘が兄の婚約に混乱して、淫乱な生活を
　送る所まで十分楽しんだからな、そろそろ家来ごっこは
　お終いにしたのさ」
> CONTEXT: Map010/events/11/pages/0/61/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そんな……酷い……！ 」
> CONTEXT: Map010/events/11/pages/0/67/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「酷い？ 感謝して欲しいくらいさ！ 私が手を加えなければ
　お前たちはただ幸せに兄妹で愛し合い結婚しただろう！
　それを正す機会を与えたではないか！ 」
> CONTEXT: Map010/events/11/pages/0/70/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「絶対……許さないんだからっ！ 」
> CONTEXT: Map010/events/11/pages/0/76/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　クエスト５３　達成！
> CONTEXT: Map010/events/11/pages/0/95/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「さぁ、お兄ちゃんを返して！ 」
> CONTEXT: Map010/events/11/pages/1/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「頭の悪い娘だなぁ……お遊びと言ったじゃないか…… 」
> CONTEXT: Map010/events/11/pages/1/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「ふぅ～……久々にこの姿に戻るな、吸血鬼の姿は窮屈であったが
　暇つぶしとしては実に良かったぞ」
> CONTEXT: Map010/events/11/pages/1/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「なっ……なんなのアンタ！？ 」
> CONTEXT: Map010/events/11/pages/1/26/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「お嬢様、セックスばかりなさっていないで、本も読まれないと
　いけませんね～？ 悪魔も知らないとはまったく…… 」
> CONTEXT: Map010/events/11/pages/1/29/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あ……くま……？ 」
> CONTEXT: Map010/events/11/pages/1/34/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「私はお前たち吸血鬼よりもずっと長く生き、退屈なんだよ。
　もう数百年はお前達で遊ばせて貰いたいもんでね…… 」
> CONTEXT: Map010/events/11/pages/1/37/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「まずは貴様の大事な兄上様とやらの為に、どうすべきかな？
　お前が私の玩具になれば、他の者は無事に済むかもしれんな？ 」
> CONTEXT: Map010/events/11/pages/1/42/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「そんな事はさせません！！ \\N[1]様、ここは
　ひとまずお逃げに！ 」
> CONTEXT: Map010/events/11/pages/1/56/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「でもっ……お兄ちゃんがっ……！ 」
> CONTEXT: Map010/events/11/pages/1/61/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「モリィ！！！ 」
> CONTEXT: Map010/events/11/pages/1/75/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「\\N[1]ちゃん、いったん逃げるよ！ 」
> CONTEXT: Map010/events/11/pages/1/87/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「でも……っ 」
> CONTEXT: Map010/events/11/pages/1/90/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「\\N[1]様、じっとして…… 」
> CONTEXT: Map010/events/11/pages/1/99/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「わざわざお越しになるとは、お嬢様に申し訳ありませんなぁ」
> CONTEXT: Map010/events/11/pages/3/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「今更演技はいらないわよ？ 」
> CONTEXT: Map010/events/11/pages/3/12/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「フン。従順な家来の役も悪くはなかったが……
　これからは私が主人だ。覚悟はできたかね？ 」
> CONTEXT: Map010/events/11/pages/3/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「その件ならお断りよ！ アンタを倒してお兄ちゃんも
　この城も返してもらうわ！ 」
> CONTEXT: Map010/events/11/pages/3/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「吸血鬼ごときが、勝てる気で来たとは、冗談でも
　笑えんが……いいだろう、自分の弱さを思い知るがいい！ 」
> CONTEXT: Map010/events/11/pages/3/31/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「これでわかった？ 私だって、やる時はやるんだからっ！ 」
> CONTEXT: Map010/events/11/pages/4/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「ちっ、生意気な女だ……。だがいいさ、お前の事は諦めよう。
　世界は広い――他でいくらでも遊べるからな」
> CONTEXT: Map010/events/11/pages/4/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　　クエスト６１　達成！
> CONTEXT: Map010/events/11/pages/4/24/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「案外あっさりと引き下がりましたね…… 」
> CONTEXT: Map010/events/11/pages/5/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「悪魔は気まぐれだ。反抗されて飽きたのかもしれんな……
　気まぐれゆえ、安心はできんが…… 」
> CONTEXT: Map010/events/11/pages/5/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃん……！ 」
> CONTEXT: Map010/events/11/pages/5/20/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「よく頑張ったな。お前を誇りに思うよ」
> CONTEXT: Map010/events/11/pages/5/29/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「だって……お兄ちゃんのためだもん！ 私なんだって……っ 」
> CONTEXT: Map010/events/11/pages/5/33/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……っ 」
> CONTEXT: Map010/events/11/pages/5/39/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「…… 」
> CONTEXT: Map010/events/11/pages/5/42/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「……今はやめておこう」
> CONTEXT: Map010/events/11/pages/5/46/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……？ キスしてくれないのぉ？ 」
> CONTEXT: Map010/events/11/pages/5/51/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「可愛い妹をいつまでも手の中に入れておきたがったが
　一生という訳にはいかないな。……お前も大人の女性だ」
> CONTEXT: Map010/events/11/pages/5/54/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「私もお前の自立を認める時期だったんだな…… 」
> CONTEXT: Map010/events/11/pages/5/59/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そう言われちゃうと、なんか寂しい…… 」
> CONTEXT: Map010/events/11/pages/5/63/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「兄として傍にいる事には変わりないさ。モリィも
　お前がどんな選択をしても、傍にいるだろうな」
> CONTEXT: Map010/events/11/pages/5/66/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……選択…… 」
> CONTEXT: Map010/events/11/pages/5/72/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「ぼっ、僕も！ 僕も\\N[1]ちゃんの幼馴染として
　ずっと傍にいます！ 」
> CONTEXT: Map010/events/11/pages/5/84/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「ふふふ、\\N[1]ちゃんは幸せ者ね。
　でも\\C[2]婚約指輪\\C[0]を渡せるのはひとりだけよ？ 」
> CONTEXT: Map010/events/11/pages/5/98/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「あなたの気持ちが決まったら、私に\\C[2]指輪を渡す相手\\C[0]を教えてね」
> CONTEXT: Map010/events/11/pages/5/106/Dialogue < UNTRANSLATED
> CONTEXT: Map010/events/19/pages/0/415/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「お前にも十分資格がある。これまでよく\\N[1]を
　サポートしてくれたな。感謝するぞ」
> CONTEXT: Map010/events/11/pages/5/116/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「わ……私が！？ おぉ……なんたる光栄でしょう！
　ヴィクター様にお褒め頂いただけでももったいないくらいです！ 」
> CONTEXT: Map010/events/11/pages/5/121/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[2]クエスト６２　恋の行方\\C[0]
発生しました。
> CONTEXT: Map010/events/11/pages/5/135/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「お前をひとりの女性として愛してる事に気づかされた。
　だが、お前の幸福がなによりの望み。好きな道を進みなさい」
> CONTEXT: Map010/events/16/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「小さい頃から、君の事見てるからね、君が幸せになるのを
　見届けられるだけで、僕は嬉しいよ。
　\\N[1]ちゃんらしさを見せてね」
> CONTEXT: Map010/events/17/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「私が選択肢に入っているとは、まことに恐縮です！
　\\N[1]様への忠誠心なら誰にも負けません！ どなたを
　お選びになられても、この気持ちは変わりません！ 」
> CONTEXT: Map010/events/18/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「もう心は決まったのかしら？ この選択は１度きりよ？
　心の準備はできた？ よ～く考えてね」
> CONTEXT: Map010/events/19/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
お兄ちゃんを選ぶ！
> CONTEXT: Map010/events/19/pages/0/10/Choice/0 < UNTRANSLATED

> END STRING

> BEGIN STRING
ロイドを選ぶ！
> CONTEXT: Map010/events/19/pages/0/10/Choice/1 < UNTRANSLATED

> END STRING

> BEGIN STRING
モリィを選ぶ！
> CONTEXT: Map010/events/19/pages/0/10/Choice/2 < UNTRANSLATED

> END STRING

> BEGIN STRING
誰も選ばない！
> CONTEXT: Map010/events/19/pages/0/10/Choice/3 < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃん、私……やっぱりお兄ちゃんが好き……
　ずっと一緒に居たいよ…… 」
> CONTEXT: Map010/events/19/pages/0/59/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「もう何も言うな。私こそ、お前の気持ちを誤解していたようだ」
> CONTEXT: Map010/events/19/pages/0/64/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ロイド……小さい頃からずっと見守ってくれてたのね。
　そして私を想ってくれてた……」
> CONTEXT: Map010/events/19/pages/0/116/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「今まで気が付かなくてごめんね。
　…………まだ……私の事好き？ 」
> CONTEXT: Map010/events/19/pages/0/121/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「勿論です！ 僕は今も……いつでもこれからも！
　ずっと\\N[1]ちゃんを愛しているよ！ 」
> CONTEXT: Map010/events/19/pages/0/125/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ロイド……嬉しい……ねぇ、来て\\I[122] 」
> CONTEXT: Map010/events/19/pages/0/131/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「やっぱ私の横には、アンタが一番似合うわね」
> CONTEXT: Map010/events/19/pages/0/183/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「\\N[1]様……私なんかをお選びになさるなんて……
　本当に良いのですか！？ ヴィクター様もロイド様も
　あなた様を…… 」
> CONTEXT: Map010/events/19/pages/0/189/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「黙ってモリィ！ 私が決めた事にケチつけるつもり？ 」
> CONTEXT: Map010/events/19/pages/0/195/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「そそそ、そんな滅相もない！ 」
> CONTEXT: Map010/events/19/pages/0/199/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「でしょ\\I[122] だったら黙って、この\\N[1]様の
　言う通りにしてればいいの\\I[122] 」
> CONTEXT: Map010/events/19/pages/0/204/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「いこっか\\I[122] 」
> CONTEXT: Map010/events/19/pages/0/209/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……ふん。私は誰も選ばない…… 」
> CONTEXT: Map010/events/19/pages/0/246/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「何を不貞腐れているのだ、ワガママ言ってないで
　お前の伴侶を選びなさい」
> CONTEXT: Map010/events/19/pages/0/249/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「嫌よ…… 」
> CONTEXT: Map010/events/19/pages/0/254/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「\\N[1]ちゃん……そんな事言わないで。
　みんな君を大切に想ってるのに…… 」
> CONTEXT: Map010/events/19/pages/0/257/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「そうですよ、\\N[1]様！ もうお疲れでしょう？
　城でゆっくり暮らしましょう！ ね？ 」
> CONTEXT: Map010/events/19/pages/0/262/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ふーんだ。私は疲れてなんかないわ！ むしろ城を出て
　食事には不自由しないし、満足してるわ」
> CONTEXT: Map010/events/19/pages/0/267/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「どうせ特定の誰かと想い合うなんて、私にはできないんだわ！
　だって……私は―― 」
> CONTEXT: Map010/events/19/pages/0/272/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「淫乱な吸血鬼だもの！ 」
> CONTEXT: Map010/events/19/pages/0/277/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「どうするのモリィ！？ 付いてくる？ 私、人間の町で
　もぉ～っと淫乱に暮らしちゃうけど！ 」
> CONTEXT: Map010/events/19/pages/0/287/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「私はっ……私は\\N[1]様がどのような選択をしようと
　どこまでもお供いたします！ 忠実な家来として！ 」
> CONTEXT: Map010/events/19/pages/0/291/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そうこなくっちゃ\\I[122] いくわよ、モリィ\\I[122] 」
> CONTEXT: Map010/events/19/pages/0/295/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「……困った子だ」
> CONTEXT: Map010/events/19/pages/0/395/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「ははは……嫌われちゃった……かな…… 」
> CONTEXT: Map010/events/19/pages/0/398/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ヴィクター
\\C[0]「そのうち機嫌が直るさ…… 」
> CONTEXT: Map010/events/19/pages/0/401/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
ちょっと時間をちょうだい
> CONTEXT: Map010/events/19/pages/0/412/Choice/0 < UNTRANSLATED

> END STRING
