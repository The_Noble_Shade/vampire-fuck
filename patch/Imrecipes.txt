> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
アロエベラ
> CONTEXT: Imrecipes/1/name/ < UNTRANSLATED
> CONTEXT: Imrecipes/5/name/ < UNTRANSLATED
> CONTEXT: Imrecipes/6/name/ < UNTRANSLATED
Aloe Vera
> END STRING

> BEGIN STRING
ブドウジュース
> CONTEXT: Imrecipes/2/name/ < UNTRANSLATED
Grape Juice
> END STRING

> BEGIN STRING
オリデオコン
> CONTEXT: Imrecipes/3/name/ < UNTRANSLATED
> CONTEXT: Imrecipes/14/name/ < UNTRANSLATED
Oligodecone
> END STRING

> BEGIN STRING
エルニウム
> CONTEXT: Imrecipes/4/name/ < UNTRANSLATED
> CONTEXT: Imrecipes/15/name/ < UNTRANSLATED
Elonium
> END STRING

> BEGIN STRING
チーズ
> CONTEXT: Imrecipes/7/name/ < UNTRANSLATED
Cheese
> END STRING

> BEGIN STRING
アシッドボトル
> CONTEXT: Imrecipes/8/name/ < UNTRANSLATED
Acid Bottle
> END STRING

> BEGIN STRING
身長変化ポーション
> CONTEXT: Imrecipes/9/name/ < UNTRANSLATED
Height Change Potion
> END STRING

> BEGIN STRING
ステリセスティック
> CONTEXT: Imrecipes/10/name/ < UNTRANSLATED
Twin Stick
> END STRING

> BEGIN STRING
キラキラスティック
> CONTEXT: Imrecipes/11/name/ < UNTRANSLATED
Glittering Stick
> END STRING

> BEGIN STRING
オリデオコン原石
> CONTEXT: Imrecipes/12/name/ < UNTRANSLATED
Organic Ore
> END STRING

> BEGIN STRING
エルニウム原石
> CONTEXT: Imrecipes/13/name/ < UNTRANSLATED
Elonium Ore
> END STRING

> BEGIN STRING
一反木綿カード
> CONTEXT: Imrecipes/16/name/ < UNTRANSLATED
Cotton Bolt Card
> END STRING

> BEGIN STRING
ヒドラカード　1000z
> CONTEXT: Imrecipes/20/name/ < UNTRANSLATED
Hydra Card 1000z
> END STRING

> BEGIN STRING
普段着
> CONTEXT: Imrecipes/21/name/ < UNTRANSLATED
> CONTEXT: Armors/100/name/ < UNTRANSLATED
Casual Clothes
> END STRING

> BEGIN STRING
普段着をポーションをと換します。
> CONTEXT: Imrecipes/21/description/ < UNTRANSLATED
Exchange Potions for Casual Clothes.
> END STRING

> BEGIN STRING
バンダナ
> CONTEXT: Imrecipes/22/name/ < UNTRANSLATED
> CONTEXT: Armors/105/name/ < UNTRANSLATED
Bandana
> END STRING

> BEGIN STRING
バンダナをポーションと交換します。
> CONTEXT: Imrecipes/22/description/ < UNTRANSLATED
Exchange potions for a Bandana.
> END STRING

> BEGIN STRING
幸運のお守り
> CONTEXT: Imrecipes/23/name/ < UNTRANSLATED
> CONTEXT: Armors/154/name/ < UNTRANSLATED
Good Luck Charm
> END STRING

> BEGIN STRING
幸運のお守りをラックアップと交換します。
> CONTEXT: Imrecipes/23/description/ < UNTRANSLATED
Exchange a Luck Up for a Good Luck Charm.
> END STRING

> BEGIN STRING
光のタリスマン
> CONTEXT: Imrecipes/24/name/ < UNTRANSLATED
> CONTEXT: Armors/159/name/ < UNTRANSLATED
Light Talisman
> END STRING

> BEGIN STRING
光のタリスマンをいろいろと交換します。
> CONTEXT: Imrecipes/24/description/ < UNTRANSLATED

> END STRING

> BEGIN STRING
回復セット
> CONTEXT: Imrecipes/25/name/ < UNTRANSLATED
Recovery Set
> END STRING

> BEGIN STRING
ポーション、マジックウォーター、アンチドーテのセット
> CONTEXT: Imrecipes/25/description/ < UNTRANSLATED
Potion, Magic Water, Antidote set
> END STRING

> BEGIN STRING
勇者セット
> CONTEXT: Imrecipes/26/name/ < UNTRANSLATED
Hero Set
> END STRING

> BEGIN STRING
初めての勇者セットです。
駆け出しの勇者さんにおすすめです。
> CONTEXT: Imrecipes/26/description/ < UNTRANSLATED
Beginner Hero set.
Recommended for novice heroes.
> END STRING
